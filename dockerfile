# Utiliser l'image de base Ubuntu
FROM ubuntu:latest

# Mainteneur du Dockerfile
LABEL maintainer="votre.email@example.com"

# Mettre à jour les packages et installer les prérequis
RUN apt-get update && \
    apt-get install -y \
    apache2 \
    php \
    libapache2-mod-php \
    php-mysql \
    php-xml \
    php-mbstring \
    unzip \
    wget \
    curl \
    && apt-get clean

# Install Composer
RUN cd /tmp && curl -sS https://getcomposer.org/installer | php && \
    mv /tmp/composer.phar /usr/local/bin/composer

# Install PHPUnit
RUN cd /tmp && curl https://phar.phpunit.de/phpunit.phar > phpunit.phar && \
    chmod +x phpunit.phar && \
    mv /tmp/phpunit.phar /usr/local/bin/phpunit

# Copier les fichiers de l'application dans le répertoire de l'hôte
COPY . /var/www/html/

# Définir le répertoire de travail
WORKDIR /var/www/html/

# Installer PHPUnit via Composer
RUN composer require --dev phpunit/phpunit

# Exposer le port 80 pour Apache
EXPOSE 80

# Démarrer Apache en avant-plan
CMD ["apachectl", "-D", "FOREGROUND"]
